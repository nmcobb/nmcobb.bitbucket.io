var searchData=
[
  ['get_5fcalibration_67',['get_calibration',['../classLab4_1_1IMU.html#a959e36c37756cd6c68bbc4d1e72b2d9d',1,'Lab4::IMU']]],
  ['get_5fdelta_68',['get_delta',['../classLab2_1_1Encoder.html#aec08e5b3142e44fd68c1f6ebee7c4fc9',1,'Lab2::Encoder']]],
  ['get_5finput_69',['get_input',['../main_8py.html#ad1de0d738f87841428bde1adae3ac39e',1,'main']]],
  ['get_5forientation_70',['get_orientation',['../classLab4_1_1IMU.html#a63e15fd9cf0d88a812071969c36918a1',1,'Lab4::IMU']]],
  ['get_5fposition_71',['get_position',['../classLab2_1_1Encoder.html#ac77646cd2ce077349bc645bdb9747b5b',1,'Lab2::Encoder']]],
  ['get_5fvelocities_72',['get_velocities',['../classLab4_1_1IMU.html#a07fde6157a7e8dd04248f8c2a6a89d3a',1,'Lab4::IMU']]]
];
