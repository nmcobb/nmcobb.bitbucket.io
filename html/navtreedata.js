/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME-405", "index.html", [
    [ "Motor Driver", "index.html#lab1", null ],
    [ "Encoder", "index.html#lab2", null ],
    [ "Controller", "index.html#lab3", null ],
    [ "Final_Project", "Final_Project.html", [
      [ "Overview", "Final_Project.html#Project", null ],
      [ "DEMO", "Final_Project.html#DEMO", null ],
      [ "Nucleo", "Final_Project.html#Nucleo", null ],
      [ "RPI", "Final_Project.html#RPI", null ]
    ] ],
    [ "IMU", "IMU.html", null ],
    [ "ProportionalController", "ProportionalController.html", null ],
    [ "Project Proposal", "project_proposal.html", [
      [ "Problem Statement", "project_proposal.html#problem_statement", null ],
      [ "Collaboration Plan", "project_proposal.html#plan", null ],
      [ "Additional Necesary Components:", "project_proposal.html#components", null ],
      [ "Manufacture and Assembly Plan", "project_proposal.html#manufacture", null ],
      [ "Safety Assessment", "project_proposal.html#safety", null ],
      [ "Timeline:", "project_proposal.html#timeline", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';